/*
    SPDX-FileCopyrightText: 2022 Roman Gilg <subdiff@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/
#include "effect_screen.h"

namespace KWin
{

EffectScreen::EffectScreen(QObject* parent)
    : QObject(parent)
{
}

}
